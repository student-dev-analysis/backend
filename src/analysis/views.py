from concurrent.futures import ThreadPoolExecutor

from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from core.services.integrations.main import full_sync as full_sync_service

executor = ThreadPoolExecutor()


@csrf_exempt
def check_auth(request):
    if not request.user.is_superuser:
        return JsonResponse({"status": "error"}, status=401)
    return JsonResponse(
        {"status": "ok", "id": request.user.id, "email": request.user.username},
        headers={"X-WEBAUTH-ID": request.user.id, "X-WEBAUTH-USER": request.user.username},
    )


@csrf_exempt
def full_sync(request):
    task = executor.submit(full_sync_service)
    return HttpResponse(b"ok", status=200)
