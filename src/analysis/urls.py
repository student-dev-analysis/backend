"""analysis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.views.generic import RedirectView

from analysis.views import check_auth, full_sync
from core.admin import admin_site
from core.services.integrations.telegram_bot import handle_request

urlpatterns = [
    path("", RedirectView.as_view(url="/admin")),
    path("crm/", include("crm.urls")),
    path("auth_check/", check_auth),
    path("auth/", include("social_django.urls", namespace="social")),
    path("admin/", admin_site.urls),
    path("", include("core.urls")),
    path("webhook/worker/", full_sync),
    path("webhook/bot", handle_request),
]
