import requests
from django.conf import settings
from django.contrib.auth import login, get_user_model
from django.contrib.auth.backends import ModelBackend
from django.http import HttpRequest
from django.urls import reverse

User = get_user_model()

UNIENV_HOST = "https://uenv-core.kpfu.ru"


def _get_redirect_uri(request):
    return request.build_absolute_uri(reverse("unienv-auth-callback"))


def get_authorization_start_url(request: HttpRequest) -> str:
    """Генерирует URL для начала авторизации через UniEnv"""
    return (
        f"{UNIENV_HOST}/oauth/authorize/?"
        f"response_type=code&"
        f"client_id={settings.UNIENV_CLIENT_ID}&"
        f"redirect_uri={_get_redirect_uri(request)}"
    )


def get_token(request: HttpRequest, code: str) -> str:
    """Запрашивает токен по коду авторизации"""
    response = requests.post(
        f"{UNIENV_HOST}/oauth/token/",
        headers={"Content-Type": "application/x-www-form-urlencoded"},
        data={
            "client_id": settings.UNIENV_CLIENT_ID,
            "client_secret": settings.UNIENV_CLIENT_SECRET,
            "grant_type": "authorization_code",
            "code": code,
            "redirect_uri": _get_redirect_uri(request),
        },
    )
    response.raise_for_status()
    response_data = response.json()
    return response_data["access_token"]


def get_profile(token: str) -> dict:
    """Получает профиль"""
    response = requests.get(f"{UNIENV_HOST}/api/v1.0/profile/", headers={"Authorization": f"Bearer {token}"})
    response.raise_for_status()
    return response.json()


def login_with_unienv_user_profile(request: HttpRequest, unienv_user_profile: dict):
    """Авторизует пользователя на нашем сайте по профилю из UniEnv"""
    email = unienv_user_profile["email"]
    user, _ = User.objects.get_or_create(
        email=email,
        defaults={
            "is_superuser": True,
            "is_staff": True,
            "first_name": unienv_user_profile.get("first_name"),
            "last_name": unienv_user_profile.get("last_name"),
        },
    )
    login(request, user, backend="django.contrib.auth.backends.ModelBackend")
