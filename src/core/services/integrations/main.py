import traceback
from django.db import transaction

from core.models import Project, Member
from core.services.integrations.gitlab import GitlabIntegration, GitlabKSUIntegration
from core.services.integrations.trello import TrelloIntegration

services = (TrelloIntegration, GitlabIntegration, GitlabKSUIntegration)


def sync_members(project: Project):
    for service in services:
        service(project).sync_members()


def sync_project(project: Project):
    for service in services:
        service(project).run()


def check_auth():
    # project is not used in check_auth
    project = Project.objects.first()

    for service in services:
        try:
            service(project).check_auth()
            print(service.__name__, "ok")
        except ValueError:
            traceback.print_exc()


def full_sync(project_id=None):
    projects = Project.objects.all()
    if project_id:
        projects = projects.filter(id=project_id)

    for project in projects:
        sync_members(project)
        sync_project(project)


@transaction.atomic
def merge_members(queryset) -> Member:
    count = queryset.count()

    if count == 0:
        return queryset.none()

    members = list(queryset.all())
    member = members[0]

    if queryset.count() == 1:
        return member

    longest_name = max([m.full_name for m in members], key=len)

    ids_for_delete = set()
    for member2 in members[1:]:
        for field in Member.FIELDS_FOR_MERGE:
            member_value = getattr(member, field)
            member2_value = getattr(member2, field)

            if member_value is None and member2_value is not None:
                setattr(member, field, member2_value)
                ids_for_delete.add(member2.id)

    Member.objects.filter(id__in=ids_for_delete).delete()
    member.full_name = longest_name
    member.save()

    return member
