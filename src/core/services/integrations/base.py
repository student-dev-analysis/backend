from abc import abstractmethod

from social_django.models import UserSocialAuth

from core.models import Project


class BaseIntegration:
    provider = None

    def __init__(self, project: Project):
        self.project = project

    def _get_auth_data(self):
        if UserSocialAuth.objects.filter(provider=self.provider).exists():
            return UserSocialAuth.objects.filter(provider=self.provider).last().extra_data

    def check_auth(self):
        raise ValueError("Not implemented")

    @abstractmethod
    def sync_members(self):
        pass

    @abstractmethod
    def run(self):
        pass
