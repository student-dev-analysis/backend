import json
import logging
import requests

from pprint import pprint
from typing import Optional
from django.conf import settings

from core.models import ProjectBoard, TrelloUser, Member
from core.models.postgres import Log
from core.services.integrations.base import BaseIntegration

logger = logging.getLogger(__name__)


class TrelloIntegration(BaseIntegration):
    provider = "trello"

    def _map_action(self, action: dict) -> Optional[Log]:
        try:
            return Log(
                project_id=self.project.id,
                provider=self.provider,
                trello_board_id=action["data"]["board"]["shortLink"],
                action_id=action["id"],
                action_type=action["type"],
                date=action["date"],
                provider_member_id=action["memberCreator"]["id"],
                full_data=json.dumps(action),
            )
        except KeyError as e:
            print(action["type"])
            pprint(action)
            raise e

    def _auth_headers(self):
        data = self._get_auth_data()
        api_key = settings.SOCIAL_AUTH_TRELLO_KEY
        token = data["access_token"]["oauth_token"]

        return dict(headers={"Authorization": f'OAuth oauth_consumer_key="{api_key}", oauth_token="{token}"'})

    def _get_board_data(self, board: ProjectBoard):
        response = requests.get(f"https://trello.com/b/{board.trello_board_id}.json", **self._auth_headers())
        response.raise_for_status()
        logger.debug("data downloaded")

        return response.json()

    def _sync_item(self, board: ProjectBoard):
        logger.debug("start downloading data")

        while True:
            log_instance = (
                Log.objects.filter(
                    project_id=self.project.id,
                    provider=self.provider,
                    trello_board_id=board.trello_board_id,
                )
                .order_by("-date")
                .first()
            )

            since = None
            if log_instance is not None:
                since = getattr(log_instance, "action_id")

            url = f"https://api.trello.com/1/boards/{board.trello_board_id}/actions?limit=1000"
            if since is not None:
                url += f"&since={since}"

            response = requests.get(url, **self._auth_headers())
            response.raise_for_status()
            data = response.json()
            objects = [self._map_action(action) for action in data]
            objects = [o for o in objects if o is not None]

            Log.objects.bulk_create(objects)

            if len(objects) == 0:
                break

            logger.info(f"project {self.project} - trello: {len(objects)} inserted")

    def sync_members(self):
        for board in self.project.boards.all():
            data = self._get_board_data(board)
            trello_members_map = {member["id"]: member for member in data["members"]}
            existing_trello_user_ids = set(
                Member.objects.exclude(project_board_user__isnull=True).values_list(
                    "project_board_user__trello_id", flat=True
                )
            )
            new_members = []

            for trello_id, member in trello_members_map.items():
                if trello_id not in existing_trello_user_ids:
                    trello_user, created = TrelloUser.objects.get_or_create(
                        trello_id=trello_id,
                        defaults=dict(name=member["fullName"], login=member["username"], data=member),
                    )
                    member_model = Member(project_board_user=trello_user, full_name=trello_user.name)
                    new_members.append(member_model)

            Member.objects.bulk_create(new_members)
        logger.info(f"project {self.project} - {self.provider} members sync completed")

    def run(self):
        for board in self.project.boards.all():
            self._sync_item(board)
