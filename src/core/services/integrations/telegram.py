import json
from typing import List

from django.conf import settings
from django.utils.datetime_safe import datetime
from pytz import timezone

from core.models.postgres import Log
from core.models.project import ProjectChat, TelegramUser, Member

PROVIDER = "telegram"


def _sync_members(messages: List[dict]):
    members = {}

    for message in messages:
        user_id = int(message["from_id"].replace("user", ""))
        if user_id not in members:
            members[user_id] = message["from"]

    existing_telegram_user_ids = set(
        TelegramUser.objects.filter(telegram_id__in=members.keys()).values_list("telegram_id", flat=True)
    )

    TelegramUser.objects.bulk_create(
        [
            TelegramUser(login=name, name=name, telegram_id=user_id)
            for user_id, name in members.items()
            if user_id not in existing_telegram_user_ids
        ]
    )

    telegram_users = TelegramUser.objects.filter(telegram_id__in=members.keys())
    existing_project_chat_user_ids = Member.objects.all().values_list("project_chat_user__telegram_id", flat=True)

    new_members = []
    for telegram_user in telegram_users:
        if telegram_user.telegram_id not in existing_project_chat_user_ids:
            new_members.append(Member(full_name=telegram_user.name, project_chat_user=telegram_user))

    Member.objects.bulk_create(new_members)


def import_telegram_chat_from_file(project_chat: ProjectChat, file):
    data = json.loads(file.read())
    project_chat.telegram_chat_id = data["id"]
    project_chat.save()

    messages = [m for m in data["messages"] if m["type"] == "message"]

    _sync_members(messages)

    try:
        log_instance = (
            Log.objects.filter(
                project_id=project_chat.project.id,
                provider=PROVIDER,
                telegram_chat_id=project_chat.telegram_chat_id,
            )
            .order_by("-date")
            .first()
            .date
        )
    except Log.DoesNotExist:
        pass

    max_telegram_message_date = None
    if log_instance is not None:
        max_telegram_message_date = getattr(log_instance, "date")

    objects = []
    for message in messages:
        telegram_user_id = int(message["from_id"].replace("user", ""))
        date = datetime.strptime(message["date"], "%Y-%m-%dT%H:%M:%S").astimezone(timezone(settings.TIME_ZONE))

        if max_telegram_message_date is not None and date < max_telegram_message_date:
            continue

        log_instance = Log(
            project_id=project_chat.project_id,
            provider=PROVIDER,
            provider_member_id=str(telegram_user_id),
            telegram_chat_id=project_chat.telegram_chat_id,
            action_id=str(message["id"]),
            action_type="message",
            date=date,
            full_data=json.dumps(message),
        )
        objects.append(log_instance)

    Log.objects.bulk_create(objects)
