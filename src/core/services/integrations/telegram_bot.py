import json
import logging
import traceback
import sentry_sdk
import telebot

from datetime import datetime
from django.conf import settings
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from sentry_sdk import capture_exception
from telebot.types import Message

from core.models.postgres import Log
from core.models.project import ProjectChat, TelegramUser, Member

bot = telebot.TeleBot(settings.TELEGRAM_BOT_TOKEN, parse_mode=None)

logger = logging.getLogger(__name__)

START_TEXT = (
    "StudentDevHelperBot будет автоматически анализировать переписку в этом чате для оценки проектной активности. "
    "По всем вопросам обращайтесь к @atnartur"
)
PROVIDER = "telegram"


@bot.message_handler(commands=["start", "help"])
def _send_welcome(message):
    bot.reply_to(message, START_TEXT)


@bot.message_handler()
def _echo_all(message: Message):
    print("process", message.chat.title, "message id", message.id)

    try:
        sentry_sdk.set_context("telegram_message", message.json)
        chat_id = message.chat.id * -1
        name = message.chat.title

        if name is None:
            name = f"{message.chat.first_name} {message.chat.last_name}"

        chat, _ = ProjectChat.objects.update_or_create(telegram_chat_id=chat_id, defaults=dict(name=name))

        if chat.project_id is None:
            send_message_to_admins(
                f"Создан новый чат: {chat.name} #{chat.id}. Установите проект для него в административной панели."
            )
            return

        telegram_user, _ = TelegramUser.objects.update_or_create(
            telegram_id=message.from_user.id,
            defaults=dict(
                name=message.from_user.full_name,
                login=getattr(message.from_user, "username", message.from_user.id),
                data=message.from_user.to_json(),
            ),
        )
        Member.objects.get_or_create(project_chat_user=telegram_user, defaults=dict(full_name=telegram_user.name))

        log_instance = Log(
            project_id=chat.project_id,
            provider=PROVIDER,
            provider_member_id=str(message.from_user.id),
            telegram_chat_id=chat.telegram_chat_id,
            action_id=str(message.id),
            action_type="message",
            date=datetime.fromtimestamp(message.date),
            full_data=json.dumps(message.json),
        )
        log_instance.save()

    except Exception as e:
        print(e)
        traceback.print_exc()
        capture_exception()


def start_bot():
    logger.info("Start bot")
    bot.infinity_polling()


def send_message(chat_id, text):
    return bot.send_message(chat_id, text)


def send_message_to_admins(text):
    if settings.TELEGRAM_ADMIN_CHAT_ID is not None:
        send_message(settings.TELEGRAM_ADMIN_CHAT_ID, text)


@csrf_exempt
def handle_request(request):
    if request.method != "POST":
        return JsonResponse({"status": "ok"}, status=200)
    body = json.loads(request.body)
    update = telebot.types.Update.de_json(body)
    try:
        bot.process_new_updates([update])
        return JsonResponse({"status": "ok"}, status=200)
    except Exception as err:
        print(err)
        return JsonResponse({"status": "error"}, status=400)
