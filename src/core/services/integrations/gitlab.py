import json
import logging
import gitlab

from core.enums import ProjectRepositorySource
from core.models import ProjectRepository, Member, GitlabUser, Token
from core.models.postgres import Log
from core.services.integrations.base import BaseIntegration

logger = logging.getLogger(__name__)


class GitlabIntegration(BaseIntegration):
    provider = "gitlab"
    url = "https://gitlab.com"

    def get_repositories(self):
        return self.project.repositories.filter(source=ProjectRepositorySource.cloud)

    def _get_auth_data(self):
        return {"access_token": Token.objects.filter(service_type=self.provider).last().token}

    def _get_client(self):
        data = self._get_auth_data()
        token = data["access_token"]
        return gitlab.Gitlab(self.url, oauth_token=token)

    def check_auth(self):
        self._get_client().projects.list()

    def _sync_item(self, repo: ProjectRepository):
        gl = self._get_client()
        project = gl.projects.get(repo.gitlab_project_id)

        repo.project_path = project.path_with_namespace
        repo.gitlab_link = project.web_url
        repo.save()

        params = {}
        try:
            last_log_record = (
                Log.objects.filter(
                    project_id=self.project.id,
                    provider=self.provider,
                    gitlab_project_id=repo.gitlab_project_id,
                )
                .order_by("-date")
                .first()
            )

            if last_log_record:
                params["after"] = last_log_record.date.strftime("%Y-%m-%d")

        except Log.DoesNotExist:
            pass

        events = []
        for event in project.events.list(all=True, query_data=params):
            log_instance = Log(
                project_id=self.project.id,
                provider=self.provider,
                provider_member_id=str(event.author_id),
                gitlab_project_id=event.project_id,
                action_id=str(event.id),
                action_type=event.action_name,
                date=event.created_at,
                full_data=json.dumps(event._attrs),
            )
            events.append(log_instance)

        Log.objects.bulk_create(events)

        logger.info(f"project {self.project} - gitlab: {len(events)} inserted")

    def sync_members(self):
        for repo in self.get_repositories():
            gl = self._get_client()
            project = gl.projects.get(repo.gitlab_project_id)

            gitlab_members_map = {member.id: member for member in project.members_all.list()}
            existing_gitlab_user_ids = set(
                Member.objects.exclude(project_repository_user__isnull=True).values_list(
                    "project_repository_user__gitlab_id", flat=True
                )
            )
            new_members = []

            for gitlab_id, member in gitlab_members_map.items():
                if gitlab_id not in existing_gitlab_user_ids:
                    gitlab_user, created = GitlabUser.objects.get_or_create(
                        gitlab_id=gitlab_id, defaults=dict(name=member.name, login=member.username, data=member._attrs)
                    )
                    member_model = Member(project_repository_user=gitlab_user, full_name=gitlab_user.name)
                    new_members.append(member_model)

            Member.objects.bulk_create(new_members)
        logger.info(f"project {self.project} - {self.provider} members sync completed")

    def run(self):
        for repo in self.get_repositories():
            self._sync_item(repo)


class GitlabKSUIntegration(GitlabIntegration):
    provider = "gitlab_ksu"
    url = "https://git.kpfu.ru"

    def get_repositories(self):
        return self.project.repositories.filter(source=ProjectRepositorySource.kpfu)

    def run(self):
        for repo in self.get_repositories():
            self._sync_item(repo)

    def _get_auth_data(self):
        return {"access_token": Token.objects.filter(service_type=self.provider).last().token}
