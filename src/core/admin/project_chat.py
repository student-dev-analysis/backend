from django.contrib import admin
from django.forms import ModelForm, fields

from core.models.project import ProjectChat
from core.services.integrations.telegram import import_telegram_chat_from_file


class ProjectChatForm(ModelForm):
    import_file = fields.FileField(required=False)

    def save(self, commit=True):
        instance = super(ProjectChatForm, self).save(commit)
        file = self.cleaned_data["import_file"]
        if file:
            import_telegram_chat_from_file(instance, self.cleaned_data["import_file"])
        return instance

    class Meta:
        model = ProjectChat
        fields = ("name", "project", "telegram_chat_id")


class ProjectChatAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "project", "telegram_chat_id")
    search_fields = list_display
    form = ProjectChatForm
