from django.contrib import messages, admin
from django.contrib.admin import ModelAdmin
from django.urls import reverse
from django.utils.safestring import mark_safe

from core.services.integrations.main import merge_members


@admin.display(description="Merge members")
def merge_members_action(modeladmin, request, queryset):
    member = merge_members(queryset)
    modeladmin.message_user(
        request,
        mark_safe(
            f"{len(queryset)} members merged to <a href='{reverse('admin:core_member_change', args=(member.id,))}'>{member}</a>"
        ),
        messages.SUCCESS,
    )


class MemberAdmin(ModelAdmin):
    list_display = (
        "full_name",
        "project_repository_user",
        "project_board_user",
        "project_chat_user",
        "created_at",
    )
    actions = [merge_members_action]
    search_fields = ("full_name", "email", "gmail")
    list_filter = ("group", "is_student", "is_staff")

    def get_queryset(self, request):
        return (
            super(MemberAdmin, self)
            .get_queryset(request)
            .select_related("project_repository_user", "project_board_user", "project_chat_user")
        )
