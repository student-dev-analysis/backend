from django import forms
from django.contrib import admin, messages

from core.models.integrations import Token
from core.models.project import ProjectRepository, ProjectBoard, ProjectChat
from core.tasks import sync_project, sync_members


class ProjectRepositoryInline(admin.TabularInline):
    model = ProjectRepository
    readonly_fields = ("project_path", "gitlab_link")


class ProjectBoardInline(admin.TabularInline):
    model = ProjectBoard


class ProjectChatInline(admin.TabularInline):
    model = ProjectChat
    show_change_link = True


def sync_project_action(modeladmin, request, queryset):
    for item in queryset:
        sync_project.delay(item.id)
    modeladmin.message_user(
        request,
        f"{len(queryset)} project's data sync started",
        messages.SUCCESS,
    )


def sync_members_action(modeladmin, request, queryset):
    for item in queryset:
        sync_members.delay(item.id)
    modeladmin.message_user(
        request,
        f"{len(queryset)} project's members sync started",
        messages.SUCCESS,
    )


class ProjectAdmin(admin.ModelAdmin):
    list_display_links = ("name",)
    list_display = (
        "name",
        "course",
        "status",
        "importance",
        "ready_to_start_level",
        "customer_satisfaction_level",
        "completeness_level",
        "updated_at",
    )
    search_fields = ("name", "description")
    inlines = (ProjectRepositoryInline, ProjectBoardInline, ProjectChatInline)
    actions = [sync_project_action, sync_members_action]
    sync_project_action.short_description = "Sync data"
    sync_members_action.short_description = "Sync members"
    list_filter = (
        "status",
        "importance",
        "course",
    )
    list_editable = (
        "course",
        "status",
        "importance",
        "ready_to_start_level",
        "customer_satisfaction_level",
        "completeness_level",
    )


class TokenAddForm(forms.ModelForm):
    class Meta:
        model = Token
        fields = ("service_type", "token")


class TokenForm(forms.ModelForm):
    class Meta:
        model = Token
        exclude = ("token",)


class TokenAdmin(admin.ModelAdmin):
    list_display = ("id", "service_type", "created_at")

    def get_form(self, request, obj=None, change=False, **kwargs):
        if obj is None:
            kwargs["form"] = TokenAddForm
        else:
            kwargs["form"] = TokenForm
        return super(TokenAdmin, self).get_form(request, obj, change, **kwargs)
