from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django.contrib.auth.models import Group
from social_django.admin import UserSocialAuthOption, NonceOption, AssociationOption
from social_django.models import UserSocialAuth, Association, Nonce

from core.admin.project import ProjectAdmin, TokenAdmin
from core.admin.project_chat import ProjectChatAdmin
from core.admin.member import MemberAdmin
from core.models import Project
from core.models.integrations import Token
from core.models.project import ProjectChat, TelegramUser, Member

User = get_user_model()


class AdminSite(admin.AdminSite):
    site_title = "SDA"
    site_header = "SDA"
    index_title = "SDA"
    index_template = "core_admin/admin_index.html"


admin_site = AdminSite()
admin_site.register(User, UserAdmin)
admin_site.register(Group, GroupAdmin)

admin_site.register(UserSocialAuth, UserSocialAuthOption)
admin_site.register(Nonce, NonceOption)
admin_site.register(Association, AssociationOption)

admin_site.register(Project, ProjectAdmin)
admin_site.register(ProjectChat, ProjectChatAdmin)
admin_site.register(Member, MemberAdmin)
# admin_site.register(TelegramUser)
admin_site.register(Token, TokenAdmin)
