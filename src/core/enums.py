from django.db import models


class ProjectStatus(models.TextChoices):
    idea = "idea", "Идея"
    planning = "planning", "Планируются"
    developing = "developing", "Разработка"
    need_improvements = "need_improvements", "Ждут доработки"
    support = "support", "Поддержка"
    done = "done", "Завершены"


RUNNING_PROJECT_STATUSES = (ProjectStatus.developing, ProjectStatus.support)


class ProjectImportance(models.TextChoices):
    high = "high", "Очень важный"
    usual = "usual", "Обычный"
    no_matter = "no_matter", "Неважно"


class ProjectReadyToStartLevel(models.IntegerChoices):
    NOT_READY = 1, "Не готов к запуску"
    READY = 2, "Готов к запуску"
    STARTED = 3, "Запущен"


class ProjectRepositorySource(models.TextChoices):
    kpfu = "kpfu", "GitLab KSU"
    cloud = "cloud", "GitLab"
