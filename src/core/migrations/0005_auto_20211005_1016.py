# Generated by Django 3.2.7 on 2021-10-05 07:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0004_auto_20210922_1754"),
    ]

    operations = [
        migrations.AlterField(
            model_name="projectchat",
            name="project",
            field=models.ForeignKey(
                null=True, on_delete=django.db.models.deletion.CASCADE, related_name="chats", to="core.project"
            ),
        ),
        migrations.AlterField(
            model_name="projectchat",
            name="telegram_chat_id",
            field=models.PositiveBigIntegerField(unique=True),
        ),
    ]
