# Generated by Django 3.2.7 on 2022-01-25 10:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0014_auto_20220122_1207"),
    ]

    operations = [
        migrations.AddField(
            model_name="member",
            name="kpfu_login",
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name="Логин КФУ"),
        ),
        migrations.AlterField(
            model_name="member",
            name="comment",
            field=models.TextField(blank=True, null=True, verbose_name="Комментарий"),
        ),
        migrations.AlterField(
            model_name="member",
            name="email",
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
        migrations.AlterField(
            model_name="member",
            name="gmail",
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
        migrations.AlterField(
            model_name="member",
            name="phone",
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name="Телефон"),
        ),
    ]
