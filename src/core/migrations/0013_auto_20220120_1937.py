# Generated by Django 3.2.7 on 2022-01-20 16:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("core", "0012_alter_projectchat_telegram_chat_id"),
    ]

    operations = [
        migrations.AddField(
            model_name="project",
            name="importance",
            field=models.CharField(
                choices=[("high", "Очень важно"), ("usual", "Средне"), ("no_matter", "Неважно")],
                max_length=50,
                null=True,
                verbose_name="Важность",
            ),
        ),
        migrations.AddField(
            model_name="project",
            name="status",
            field=models.CharField(
                choices=[
                    ("idea", "Идея"),
                    ("planning", "Планируются"),
                    ("developing", "Разработка"),
                    ("need_improvements", "Ждут доработки"),
                    ("support", "Поддержка"),
                    ("done", "Завершены"),
                ],
                default="idea",
                max_length=50,
            ),
        ),
    ]
