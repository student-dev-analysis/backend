from django.core.management import BaseCommand

from core.services.integrations.main import full_sync


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--project_id", type=int)

    def handle(self, project_id=None, *args, **options):
        full_sync(project_id)
