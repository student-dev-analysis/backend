from django.core.management.base import BaseCommand
from crm.services.sync_tasks_to_trello import crm_to_trello


class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        crm_to_trello()
