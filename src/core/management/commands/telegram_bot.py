from django.core.management import BaseCommand

from core.services.integrations.telegram_bot import start_bot


class Command(BaseCommand):
    help = "start Telegram bot"

    def handle(self, *args, **options):
        start_bot()
