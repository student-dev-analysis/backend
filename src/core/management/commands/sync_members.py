from django.core.management import BaseCommand

from core.models import Project
from core.services.integrations.main import sync_members


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--project_id", type=int)

    def handle(self, project_id, *args, **options):
        project = Project.objects.get(id=project_id)
        sync_members(project)
