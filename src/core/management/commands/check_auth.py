from django.core.management import BaseCommand

from core.models import Project
from core.services.integrations.main import check_auth


class Command(BaseCommand):
    def handle(self, *args, **options):
        check_auth()
