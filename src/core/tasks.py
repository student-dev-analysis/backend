from django.contrib.auth import get_user_model

from analysis.celery import app
from core.models import Project
from core.services.integrations.main import (
    sync_project as sync_project_service,
    sync_members as sync_members_service,
    full_sync as full_sync_service,
)

User = get_user_model()


@app.task(queue="default")
def sync_project(project_id: int):
    project = Project.objects.get(id=project_id)
    sync_project_service(project)


@app.task(queue="default")
def sync_members(project_id: int):
    project = Project.objects.get(id=project_id)
    sync_members_service(project)


@app.task(queue="default")
def full_sync():
    full_sync_service()
