"""analysis URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth.views import LogoutView
from django.urls import path
from django.views.generic import TemplateView

from core.views import unienv_auth_callback, main_view, unienv_login_view

urlpatterns = [
    path("", main_view, name="main"),
    path("login/", TemplateView.as_view(template_name="web/login.html"), name="login"),
    path("login/unienv/", unienv_login_view, name="login-unienv"),
    path("login/unienv_auth_callback/", unienv_auth_callback, name="unienv-auth-callback"),
    path("logout/", LogoutView.as_view(), name="logout"),
]
