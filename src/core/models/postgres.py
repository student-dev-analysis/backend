from django.db import models


class Log(models.Model):
    project_id = models.PositiveIntegerField()
    provider = models.CharField(max_length=255)
    provider_member_id = models.CharField(max_length=255)
    trello_board_id = models.CharField(max_length=255, null=True, blank=True)
    gitlab_project_id = models.BigIntegerField(null=True, blank=True)
    telegram_chat_id = models.BigIntegerField(null=True, blank=True)
    action_id = models.CharField(max_length=255)
    action_type = models.CharField(max_length=255)
    date = models.DateTimeField()
    full_data = models.TextField()

    @classmethod
    def table_name(cls):
        return "logs"
