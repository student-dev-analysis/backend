from django.db import models

from core.models.base import BaseModel


class ExternalServiceUser(BaseModel):
    name = models.CharField(max_length=200)
    login = models.CharField(max_length=200)
    data = models.JSONField(default=dict)

    class Meta:
        abstract = True


class TelegramUser(ExternalServiceUser):
    telegram_id = models.PositiveBigIntegerField()

    def __str__(self):
        return f"{self.name} ({self.telegram_id})"


class GitlabUser(ExternalServiceUser):
    gitlab_id = models.PositiveBigIntegerField()

    def __str__(self):
        return f"{self.name} ({self.gitlab_id})"


class TrelloUser(ExternalServiceUser):
    trello_id = models.CharField(max_length=50)

    def __str__(self):
        return f"{self.name} ({self.trello_id})"


class Token(BaseModel):
    class TokenServiceType(models.TextChoices):
        gitlab = "gitlab", "gitlab"
        gitlab_ksu = "gitlab_ksu", "gitlab_ksu"

    service_type = models.CharField(choices=TokenServiceType.choices, max_length=50)
    token = models.CharField(max_length=200)
