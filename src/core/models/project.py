from django.contrib.auth import get_user_model
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from core.enums import ProjectStatus, ProjectImportance, ProjectReadyToStartLevel, ProjectRepositorySource
from core.models.base import BaseModel
from core.models.integrations import TrelloUser, TelegramUser, GitlabUser

User = get_user_model()


class ProjectTechnology(BaseModel):
    name = models.CharField(verbose_name="Название технологии", max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "технология"
        verbose_name_plural = "технологии"


class Project(BaseModel):
    name = models.CharField(max_length=200)
    description = models.TextField(null=True, blank=True)
    course = models.PositiveSmallIntegerField(null=True, blank=True)
    status = models.CharField(choices=ProjectStatus.choices, default=ProjectStatus.idea, max_length=50)
    importance = models.CharField(
        verbose_name="Важность", choices=ProjectImportance.choices, null=True, blank=True, max_length=50
    )
    employee_curator = models.ForeignKey(
        User, on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Сотрудник куратор"
    )
    links = models.TextField(null=True, blank=True, verbose_name="Ссылки на проект")
    requirements_link = models.URLField(null=True, blank=True, verbose_name="Ссылка на требования")
    technologies = models.ManyToManyField(ProjectTechnology, blank=True)

    ready_to_start_level = models.IntegerField(
        choices=ProjectReadyToStartLevel.choices, verbose_name="Готовность к запуску", null=True, blank=True
    )
    customer_satisfaction_level = models.SmallIntegerField(
        verbose_name="Уровень удовлетворенности заказчика",
        null=True,
        blank=True,
        validators=[MinValueValidator(1), MaxValueValidator(5)],
    )
    completeness_level = models.SmallIntegerField(
        verbose_name="Уровень завершенности проекта",
        null=True,
        blank=True,
        validators=[MinValueValidator(1), MaxValueValidator(5)],
    )

    @property
    def status_text(self):
        return ProjectStatus[self.status].label

    def __str__(self):
        return self.name


class ProjectRepository(BaseModel):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="repositories")
    source = models.CharField(
        choices=ProjectRepositorySource.choices,
        verbose_name="Source репозитория",
        default=ProjectRepositorySource.kpfu,
        max_length=16,
    )
    gitlab_project_id = models.IntegerField()
    project_path = models.CharField(max_length=500, null=True, blank=True, help_text="Синхронизируется автоматически")
    gitlab_link = models.URLField(null=True, blank=True, help_text="Синхронизируется автоматически")

    @property
    def link(self):
        return self.gitlab_link


class ProjectBoard(BaseModel):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="boards")
    trello_board_id = models.CharField(max_length=50)

    @property
    def link(self):
        return f"https://trello.com/b/{self.trello_board_id}"


class ProjectChat(BaseModel):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name="chats", null=True)
    name = models.CharField(max_length=500)
    telegram_chat_id = models.BigIntegerField(unique=True)

    @property
    def link(self):
        return None

    def __str__(self):
        return f"{self.name} ({self.telegram_chat_id})"


class Member(BaseModel):
    FIELDS_FOR_MERGE = (
        "group",
        "subdivision",
        "is_student",
        "is_staff",
        "kpfu_login",
        "email",
        "gmail",
        "phone",
        "link",
        "comment",
        "full_name",
        "project_repository_user",
        "project_board_user",
        "project_chat_user",
    )

    full_name = models.CharField(max_length=500, verbose_name="ФИО")
    group = models.CharField(max_length=10, null=True, blank=True, verbose_name="Группа студента")
    subdivision = models.CharField(max_length=500, null=True, blank=True, verbose_name="Подразделение сотрудника")
    is_student = models.BooleanField(default=True, verbose_name="Студент")
    is_staff = models.BooleanField(default=False, verbose_name="Сотрудник")
    kpfu_login = models.CharField(null=True, blank=True, verbose_name="Логин КФУ", max_length=50)
    email = models.EmailField(blank=True, null=True)
    gmail = models.EmailField(blank=True, null=True)
    phone = models.CharField(blank=True, null=True, verbose_name="Телефон", max_length=50)
    link = models.URLField(blank=True, null=True, verbose_name="Ссылка")
    comment = models.TextField(verbose_name="Комментарий", blank=True, null=True)
    project_repository_user = models.OneToOneField(
        GitlabUser, related_name="member", on_delete=models.SET_NULL, null=True, blank=True
    )
    project_board_user = models.OneToOneField(
        TrelloUser, related_name="member", on_delete=models.SET_NULL, null=True, blank=True
    )
    project_chat_user = models.OneToOneField(
        TelegramUser, related_name="member", on_delete=models.SET_NULL, null=True, blank=True
    )

    def __str__(self):
        return self.full_name
