from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.http import HttpResponse

from core.services.integrations.unienv import (
    get_token,
    get_profile,
    login_with_unienv_user_profile,
    get_authorization_start_url,
)


@login_required
def main_view(request):
    return render(request, "web/main.html")


def unienv_login_view(request):
    return redirect(get_authorization_start_url(request))


def unienv_auth_callback(request):
    code = request.GET.get("code")
    token = get_token(request, code)
    profile = get_profile(token)
    if profile.get("is_staff"):
        login_with_unienv_user_profile(request, profile)
        return redirect("main")
    return HttpResponse("Forbidden", content_type="text/plain", status=403)
