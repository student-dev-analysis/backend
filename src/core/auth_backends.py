from social_core.backends.trello import TrelloOAuth


class TrelloWithScopesOAuth(TrelloOAuth):
    def auth_extra_arguments(self):
        return {**super().auth_extra_arguments(), "scope": self.setting("SCOPES", "read")}
