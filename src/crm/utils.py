import re

from django.urls import reverse
from django.utils.formats import date_format
from django.utils.safestring import mark_safe

from core.enums import ProjectStatus, ProjectImportance
from crm.models import CrmProject


def html_links(text):
    return re.sub(r"(https?:\/\/[\w\-\/#=\.]+)", r'<a href="\1" target="_blank">\1</a>', text)


def get_project_colored_name(instance: CrmProject, without_link=False):
    url = reverse("admin:crm_crmproject_change", args=(instance.id,)) if not without_link else ""
    importance_text = ProjectImportance[instance.importance].label if instance.importance is not None else "-"
    instance_cls = f"importance-{instance.importance}"
    return f'<b><a href="{url}" class="{instance_cls}" title="{importance_text}">{instance.name}</a></b>'


def get_project_name_cell(instance: CrmProject, with_tasks_link=False):
    html = f"{get_project_colored_name(instance)}<br>"
    html += f'<span class="status-{instance.status}" title="Статус">{ProjectStatus[instance.status].label}</span> '
    if instance.course is not None:
        html += f'– <span class="text-normal">{instance.course} курс</span>'

    if with_tasks_link:
        tasks_link = reverse("admin:crm_projecttask_changelist") + f"?project__id__exact={instance.id}"
        html += f'<br><a href="{tasks_link}">Задачи</a>'

    html += "<br>"
    html += (
        f"<small>"
        + (f"Куратор: {instance.employee_curator}" if instance.employee_curator else "Нет куратора")
        + f"</small><br>"
    )
    html += f'<small>Обновлен {date_format(instance.updated_at, "DATETIME_FORMAT")}</small>'
    return mark_safe(html)


def get_project_services_links(instance: CrmProject):
    html = ""
    for board in instance.boards.all():
        html += (
            f'<a href="{board.link}" target="_blank" title="Trello" style="font-size: 18px;">'
            f'<i class="fab fa-trello"></i>'
            f"</a> "
        )
    for repo in instance.repositories.all():
        if repo.link is None:
            continue
        html += (
            f'<a href="{repo.link}" target="_blank" title="{repo.project_path}" style="font-size: 18px;">'
            f'<i class="fab fa-gitlab"></i>'
            f"</a> "
        )
    return mark_safe(html)


def get_project_members_cell(instance: CrmProject):
    html = ""
    for member in instance.projectmember_set.filter(is_old=False):
        url = reverse("admin:crm_crmmember_change", args=(member.member_id,))
        html += f'<a href="{url}" target="_blank">{member.member}</a><br>'
    return mark_safe(html)


def get_project_stakeholders_cell(instance: CrmProject):
    html = ""
    for stakeholder in instance.projectstakeholder_set.all():
        url = reverse("admin:crm_crmmember_change", args=(stakeholder.member_id,))
        html += f'<a href="{url}" target="_blank">{stakeholder.member}</a><br>'
    return mark_safe(html)
