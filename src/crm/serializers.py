from rest_framework import serializers


class ProjectSummaryStatsSerializer(serializers.Serializer):
    project_id = serializers.IntegerField()
    member_full_name = serializers.CharField()
    provider = serializers.CharField()
    week_start_date = serializers.CharField()
    count = serializers.IntegerField()


class ProjectSummarySerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()
    stats = ProjectSummaryStatsSerializer(many=True)
