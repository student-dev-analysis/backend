from django.db.models import Prefetch, Func
from django.utils.datetime_safe import datetime

from core.enums import RUNNING_PROJECT_STATUSES
from crm.models import CrmProject, ProjectTask


class ToMonday(Func):
    template = "%(function)s('week', %(expressions)s)"
    function = "date_trunc"


SEMESTER_START = datetime(year=2022, month=2, day=8)


def get_info():
    projects = CrmProject.objects.filter(status__in=RUNNING_PROJECT_STATUSES).prefetch_related(
        Prefetch(
            "tasks",
            ProjectTask.objects.filter(created_at__gte=SEMESTER_START)
            .annotate(week_start_date=ToMonday("created_at"))
            .order_by("created_at"),
        )
    )

    week_start_dates = set()

    for project in projects:
        tasks = {}
        for task in project.tasks.all():
            if task.week_start_date not in tasks:
                tasks[task.week_start_date] = []
                week_start_dates.add(task.week_start_date)
            tasks[task.week_start_date].append(task)
        project.grouped_tasks = tasks

        project.grouped_tasks_avg = {}
        for date, tasks in project.grouped_tasks.items():
            project.grouped_tasks_avg[date] = round(sum(t.mark or 0 for t in tasks) / len(tasks), 2)

        project.grouped_tasks_sum = round(sum(project.grouped_tasks_avg.values()), 2)
        try:
            project.grouped_tasks_sum_avg = round(
                sum(project.grouped_tasks_avg.values()) / len(project.grouped_tasks_avg), 2
            )
        except ZeroDivisionError:
            project.grouped_tasks_sum_avg = 0

    return {"projects": projects, "week_start_dates": week_start_dates}
