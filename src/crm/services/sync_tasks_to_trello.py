from django.conf import settings
import requests
from social_django.models import UserSocialAuth
from core.models import Project
from core.models import ProjectBoard
from crm.models import ProjectTask


def trello_request(method, url, query={}, **kwargs):
    """
    Возвращает код запроса к Trello

    :param method:POST/GET, query:данный для подключения API
    :type method: str, url: str, query: dict
    """
    auth_data = UserSocialAuth.objects.filter(provider="trello").last().extra_data
    key = settings.SOCIAL_AUTH_TRELLO_KEY
    token = auth_data["access_token"]["oauth_token"]
    authorization = {"key": key, "token": token}
    query.update(authorization)

    response = requests.request(method=method, url=url, headers={"Accept": "application/json"}, params=query, **kwargs)
    response.raise_for_status()
    return response


def get_key():
    """Возвращает ключ к Trello"""
    return settings.SOCIAL_AUTH_TRELLO_KEY


def get_token():
    """Возращает токен Trello"""
    auth_data = UserSocialAuth.objects.filter(provider="trello").last().extra_data
    return auth_data["access_token"]["oauth_token"]


def get_board(trello_id):
    """Принимает короткое id доски, и возвращает словарь с данными этой доски"""
    url = f"https://api.trello.com/1/boards/{trello_id}/lists"
    method = "GET"
    response = trello_request(method, url)
    board_list = response.json()
    return board_list


def get_backlog_board_id(board_list):
    """Принимает данные доски и возвращет id списка Backlog или None"""
    for i in board_list:
        if i["name"].lower() == "backlog":
            return i["id"]
        else:
            return None


def new_card(id_list, name):
    """Принимает id списка и название задачи и добавляет соответствующую карточку на Trello"""
    url = "https://api.trello.com/1/cards"
    method = "POST"
    query = {
        "idList": id_list,
        "name": name,
    }
    trello_request(method, url, query)


def new_list(trello_id):
    """Принимает Trello id и создает на ней список Backlog, возвращает id списка"""
    url = f"https://api.trello.com/1/boards/{trello_id}/lists"
    method = "POST"
    query = {
        "name": "Backlog",
    }
    response = trello_request(method, url, query)
    id_list = response.json()
    return id_list["id"]


def sync_project_tasks(project_id):
    trello_id = ProjectBoard.objects.get(project_id=project_id).trello_board_id
    tasks = ProjectTask.objects.filter(project_id=project_id, is_completed=False, added_to_trello=False)
    boar_list = get_board(trello_id)
    id_list = get_backlog_board_id(boar_list)
    if id_list == None:
        id_list = new_list(trello_id)
    for task in tasks:
        name = task.title
        new_card(id_list, name)
        task.added_to_trello = True
        task.save()


def crm_to_trello():
    projects = Project.objects.all()
    for project in projects:
        sync_project_tasks(project.id)
