from itertools import groupby
from collections import defaultdict

from django.db.models import Count, Q, F

from crm.models import ProjectMember, LogsWithMembers
from crm.services.analysis.base import BaseAnalysisService


class ProjectSummaryService(BaseAnalysisService):
    def __init__(self, project_id, user_id=None, **kwargs):
        self.project_id = project_id
        self.user_id = user_id
        super(ProjectSummaryService, self).__init__(**kwargs)

    def run(self):
        date_filter = self.get_dates_where()

        user_filter = Q()
        if self.user_id is not None:
            user_filter = Q(provider_member_id=self.user_id)

        weekly_logs = (
            LogsWithMembers.objects.filter(Q(project_id=self.project_id) & user_filter & date_filter)
            .annotate(annotated_week_start_date=F("week_start_date"))
            .values("project_id", "member_full_name", "provider", "annotated_week_start_date")
            .annotate(count=Count("*"))
            .order_by("-annotated_week_start_date", "member_full_name")
        )

        rows = list(weekly_logs)
        dates = set()
        member_weekly_stat = defaultdict(dict)
        for member_full_name, member_items in groupby(rows, key=lambda x: x["member_full_name"]):
            for week_start_date, weekly_items in groupby(member_items, key=lambda x: x["annotated_week_start_date"]):
                week_start_date_str = str(week_start_date)
                dates.add(week_start_date_str)
                member_weekly_stat[member_full_name][week_start_date_str] = sum(item["count"] for item in weekly_items)

        provider_weekly_stat = defaultdict(dict)
        for provider, provider_items in groupby(rows, key=lambda x: x["provider"]):
            for week_start_date, weekly_items in groupby(provider_items, key=lambda x: x["annotated_week_start_date"]):
                week_start_date_str = str(week_start_date)
                provider_weekly_stat[provider][week_start_date_str] = sum(item["count"] for item in weekly_items)

        daily_logs = (
            LogsWithMembers.objects.filter(Q(project_id=self.project_id) & user_filter & date_filter)
            .values("project_id", "member_full_name", "provider", "date")
            .annotate(count=Count("*"))
            .order_by("date", "member_full_name")
        )

        daily_dates = set()
        rows = list(daily_logs)
        member_daily_stat = defaultdict(dict)
        for member_full_name, member_items in groupby(rows, key=lambda x: x["member_full_name"]):
            for date, day_items in groupby(member_items, key=lambda x: x["date"]):
                date_str = str(date)
                daily_dates.add(date_str)
                member_daily_stat[member_full_name][date_str] = sum(item["count"] for item in day_items)

        provider_daily_stat = defaultdict(dict)
        for provider, provider_items in groupby(rows, key=lambda x: x["provider"]):
            for date, day_items in groupby(provider_items, key=lambda x: x["date"]):
                date_str = str(date)
                provider_daily_stat[provider][date_str] = sum(item["count"] for item in day_items)

        members_map = dict(
            ProjectMember.objects.filter(project_id=self.project_id)
            .select_related("member")
            .values_list("member__full_name", "member_id")
        )

        return {
            "weekly_dates": sorted(dates),
            "daily_dates": sorted(daily_dates),
            "member_weekly_stat": dict(member_weekly_stat),
            "provider_weekly_stat": dict(provider_weekly_stat),
            "member_daily_stat": dict(member_daily_stat),
            "provider_daily_stat": dict(provider_daily_stat),
            "members_map": members_map,
        }
