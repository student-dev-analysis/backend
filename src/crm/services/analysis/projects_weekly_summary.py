from itertools import groupby

from django.db.models import Q, Count

from core.enums import RUNNING_PROJECT_STATUSES
from crm.constants import PROJECT_ORDERING
from crm.models import CrmProject, LogsWithMembers
from crm.services.analysis.base import BaseAnalysisService, ProjectProviderSummaryRow


class ProjectsWeeklySummaryService(BaseAnalysisService):
    def __init__(self, project_id=None, **kwargs):
        self.project_id = project_id
        super(ProjectsWeeklySummaryService, self).__init__(**kwargs)

    def run(self):
        projects_clause = Q(status__in=RUNNING_PROJECT_STATUSES)
        if self.project_id is not None:
            projects_clause &= Q(id=self.project_id)

        projects_map = {
            p.id: {"id": p.id, "name": p.name, "stats": []}
            for p in CrmProject.objects.filter(projects_clause).order_by(*PROJECT_ORDERING)
        }
        project_ids = projects_map.keys()

        logs_with_members = (
            LogsWithMembers.objects.filter(project_id__in=project_ids, date__gt=self.start_date, date__lt=self.end_date)
            .annotate(count=Count("*"))
            .values("project_id", "provider", "week_start_date", "count", "member_full_name")
            .order_by("-week_start_date")
        )

        dates = set()
        for raw_row in logs_with_members:
            row = ProjectProviderSummaryRow(**raw_row)
            dates.add(row.week_start_date)
            projects_map[row.project_id]["stats"].append(row)

        for project in projects_map.values():
            project["stat_by_week"] = {}
            for week_start_date, items in groupby(project["stats"], key=lambda x: x.week_start_date):
                project["stat_by_week"][week_start_date] = sum(map(lambda x: int(x.count), items))

            project.pop("stats")

        return {"dates": sorted(dates), "projects": projects_map.values()}
