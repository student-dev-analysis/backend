import requests
from django.conf import settings

from core.enums import ProjectStatus
from core.models import ProjectRepository


def start_code_analysis_for_all_repos():
    gitlab_links = ProjectRepository.objects.filter(project__status=ProjectStatus.developing).values_list(
        "gitlab_link", flat=True
    )
    response = requests.post(
        f"{settings.CODE_ANALYSIS_URL}/api/repos/analysis",
        headers={"Authorization": f"Token {settings.CODE_ANALYSIS_TOKEN}"},
        data="\n".join(gitlab_links),
    )
    response.raise_for_status()
