from abc import abstractmethod
from datetime import date
from typing import Optional, NamedTuple

from django.db.models import Q


class ProjectProviderSummaryRow(NamedTuple):
    project_id: int
    member_full_name: str
    provider: str
    week_start_date: str
    count: int


class ProjectProviderRow(NamedTuple):
    project_id: int
    member_full_name: str
    provider: str
    date: str
    count: int


class BaseAnalysisService:
    def __init__(self, start_date: Optional[date] = None, end_date: Optional[date] = None):
        self.start_date = start_date
        self.end_date = end_date

    def get_dates_where(self):
        date_filter = Q()
        if self.start_date is not None:
            date_filter &= Q(date__gt=self.start_date)

        if self.end_date is not None:
            date_filter &= Q(date__lt=self.end_date)

        return date_filter

    @abstractmethod
    def run(self):
        pass
