from datetime import date
from typing import NamedTuple

from django.db.models import Q, Max

from core.enums import RUNNING_PROJECT_STATUSES
from crm.constants import PROJECT_ORDERING
from crm.models import CrmProject
from core.models.postgres import Log
from crm.services.analysis.base import BaseAnalysisService
from crm.utils import get_project_colored_name


class ProjectLastUpdateResult(NamedTuple):
    project_id: int
    provider: str
    max_date: date


def _get_link(queryset, link_attr):
    try:
        return getattr(queryset.all()[0], link_attr)
    except (AttributeError, IndexError):
        return None


class ProjectsLastUpdateSummaryService(BaseAnalysisService):
    def run(self):
        projects_clause = Q(status__in=RUNNING_PROJECT_STATUSES)
        queryset = (
            CrmProject.objects.filter(projects_clause)
            .prefetch_related("repositories", "boards")
            .annotate(tasks_updated_at=Max("tasks__updated_at"))
            .order_by(*PROJECT_ORDERING)
        )
        projects_map = {
            p.id: {
                "id": p.id,
                "name": p.name,
                "name_html": get_project_colored_name(p, without_link=True),
                "trello_link": _get_link(p.boards.all(), "link"),
                "trello_max_date": None,
                "gitlab_link": _get_link(p.repositories.all(), "link"),
                "gitlab_max_date": None,
                "telegram_max_date": None,
                "updated_at": p.updated_at,
                "tasks_updated_at": p.tasks_updated_at,
            }
            for p in queryset
        }
        project_ids = projects_map.keys()
        results = (
            Log.objects.filter(project_id__in=project_ids)
            .values("project_id", "provider")
            .annotate(max_date=Max("date"))
        )

        for raw_row in results:
            row = ProjectLastUpdateResult(*raw_row.values())
            project_dict = projects_map[row.project_id]
            project_dict[f"{row.provider}_max_date"] = row.max_date.isoformat()

        return projects_map.values()
