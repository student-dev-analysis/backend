# Generated by Django 3.2.11 on 2022-08-17 07:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("crm", "0009_auto_20220610_0847"),
    ]

    operations = [
        migrations.AddField(
            model_name="projectmember",
            name="is_old",
            field=models.BooleanField(default=False, verbose_name="Участвовал в прошлом"),
        ),
    ]
