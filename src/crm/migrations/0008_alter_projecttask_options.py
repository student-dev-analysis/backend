# Generated by Django 3.2.11 on 2022-02-25 16:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("crm", "0007_auto_20220208_2244"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="projecttask",
            options={"verbose_name": "Ключевая задача", "verbose_name_plural": "Задачи"},
        ),
    ]
