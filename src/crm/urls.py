from django.urls import path

from crm.views import (
    projects_weekly_summary_api,
    project_weekly_summary_api,
    analysis_view,
    project_info,
    projects_last_update_api,
    project_tasks_tabel_view,
    project_sync_tasks,
    start_code_analysis,
)

app_name = "crm"

urlpatterns = [
    path("analysis/", analysis_view, name="analysis"),
    path("analysis/start_code_analysis/", start_code_analysis, name="start_code_analysis"),
    path("project_tasks_tabel/", project_tasks_tabel_view, name="project_tasks_tabel"),
    path("project_info_cell/<int:project_id>/", project_info),
    path("sync_project_tasks/<int:project_id>/", project_sync_tasks, name="project_sync_tasks"),
    path("api/projects_last_update_summary/", projects_last_update_api),
    path("api/projects_weekly_summary/", projects_weekly_summary_api),
    path("api/projects_weekly_summary/<int:project_id>/", project_weekly_summary_api),
]
