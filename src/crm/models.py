from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import UniqueConstraint
from django.utils.formats import date_format

from core.models import Project, Member
from core.models.base import BaseModel
from core.models.project import ProjectTechnology

User = get_user_model()


class CrmProject(Project):
    class Meta:
        proxy = True
        verbose_name = "Проект"
        verbose_name_plural = "Проекты"


class CrmMember(Member):
    def __str__(self):
        name = self.full_name
        if self.is_staff:
            name += f" ({self.subdivision or 'сотрудник'})"
        elif self.group:
            name += f" ({self.group})"
        return name

    class Meta:
        proxy = True
        verbose_name = "Контакт"
        verbose_name_plural = "Контакты"


class CrmProjectTechnology(ProjectTechnology):
    class Meta:
        proxy = True
        verbose_name = "Технология"
        verbose_name_plural = "Технологии"


class ProjectMember(BaseModel):
    project = models.ForeignKey(CrmProject, on_delete=models.CASCADE, verbose_name="Проект")
    member = models.ForeignKey(CrmMember, on_delete=models.CASCADE, verbose_name="Контакт")
    comment = models.TextField(verbose_name="Комментарий", blank=True, null=True)
    is_old = models.BooleanField(default=False, verbose_name="Участвовал в прошлом")

    class Meta:
        verbose_name = "Участие в проекте"
        verbose_name_plural = "Участия в проектах"
        constraints = [UniqueConstraint(fields=("project", "member"), name="project_member_unique")]


class ProjectStakeholder(BaseModel):
    project = models.ForeignKey(CrmProject, on_delete=models.CASCADE, verbose_name="Проект")
    member = models.ForeignKey(CrmMember, on_delete=models.CASCADE, verbose_name="Контакт")
    comment = models.TextField(verbose_name="Комментарий", blank=True, null=True)

    class Meta:
        verbose_name = "Заинтересованное лицо"
        verbose_name_plural = "Заинтересованные лица"


class ProjectComment(BaseModel):
    project = models.ForeignKey(CrmProject, on_delete=models.CASCADE, verbose_name="Проект")
    # сначала сохраняются все модели в админке, которые есть во всех инлайнах, а вторым сохранением добавляются авторы.
    # Если это не сделать, тодга операции удаления не будут работать в других инлайнах, и это плохо
    author = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    text = models.TextField()

    def __str__(self):
        return f'{self.author} в {date_format(self.created_at, "DATETIME_FORMAT")}'

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"


class ProjectTask(BaseModel):
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(CrmProject, on_delete=models.CASCADE, verbose_name="Проект", related_name="tasks")
    title = models.CharField(max_length=500)
    date = models.DateField(null=True, blank=True, verbose_name="Крайний срок")
    mark = models.IntegerField(null=True, blank=True, verbose_name="Оценка")
    added_to_trello = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Ключевая задача"
        verbose_name_plural = "Задачи"


class LogsWithMembers(models.Model):
    """Модель получения данных для событий по неделям в аналитике."""

    project_id = models.IntegerField()
    member_full_name = models.CharField(max_length=255)
    provider = models.CharField(max_length=50)
    date = models.DateField()
    week_start_date = models.TextField()

    class Meta:
        managed = False
        db_table = "logs_with_members"
