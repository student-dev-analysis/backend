function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
}

function highlightSelectedProject() {
    const projectId = getQueryVariable('project__id__exact');
    if (projectId) {
        document.querySelector(`#projectLink${projectId}`).classList.add('active')
        document.querySelector('#task-new').style.display = '';
        document.querySelector('#task-new [name="project"]').setAttribute('value', projectId);
    }
}

function taskChangeHandler(event) {
    const form = event.target.closest('form');
    console.log(form);

    fetch(form.getAttribute('action'), {
        method: form.getAttribute('method'),
        body: new FormData(form)
    }).then(() => {
        event.target.classList.add('border-success');
        setTimeout(() => event.target.classList.remove('border-success'), 1000);
    }).catch(error => {
        console.error(error);
        alert('Произошла ошибка, попробуйте еще раз');
    });
}

function taskChangesSavesInit() {
    document.querySelectorAll('.task-row .task-content').forEach(item => {
        item.addEventListener('click', event => {
            if (event.target.tagName === 'A') {
                return; // пропускаем ссылки
            }
            event.target.style.display = 'none';
            event.target.closest('form').querySelector('[name="title"]').style.display = '';
        });
    });
    document.querySelectorAll('.task-row input, .task-row textarea').forEach(item => {
        item.addEventListener('change', taskChangeHandler);
    });
}

highlightSelectedProject();
taskChangesSavesInit();

