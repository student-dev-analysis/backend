function search() {
    const value = document.querySelector('#searchinput').value.trim().toLowerCase();
    document.querySelectorAll('#projects_sidebar a').forEach(function (element) {
        const isFound = element.textContent.trim().toLowerCase().includes(value);
        element.style.display = isFound ? '' : 'none';
    })
}

search();
document.querySelector('#searchinput').addEventListener('input', search);
