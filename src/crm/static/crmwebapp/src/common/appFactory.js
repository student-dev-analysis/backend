import { createApp } from "vue";
import { createPinia } from "pinia/dist/pinia.esm-browser";
import HighchartsVue from "highcharts-vue";
import mitt from "mitt";

const bus = mitt();

function open() {
    bus.emit("open");
    const content = document.querySelector("#djangoContent");
    if (content !== null) {
        document.querySelector("#djangoContent").style.display = "none";
    }
}

function close() {
    const content = document.querySelector("#djangoContent");
    if (content !== null) {
        document.querySelector("#djangoContent").style.display = "block";
    }
}
export default function createCrmApp(App) {
    const app = createApp(App).use(createPinia()).use(HighchartsVue);
    app.config.globalProperties.$bus = bus;
    const button = document.querySelector("#startButton");
    if (button) {
        button.addEventListener("click", open);
    }
    bus.on("close", close);
    return app;
}

// document.addEventListener("DOMContentLoaded", open);
