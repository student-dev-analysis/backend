import { defineStore } from "pinia";

const endDate = new Date();
const startDate = new Date();
startDate.setDate(-90);

export const useStore = defineStore("main", {
    state() {
        return {
            startDate: startDate.toISOString().slice(0, 10),
            endDate: endDate.toISOString().slice(0, 10),
        };
    },
    actions: {
        setStartDate(date) {
            this.startDate = date;
        },
        setEndDate(date) {
            this.endDate = date;
        },
    },
});
