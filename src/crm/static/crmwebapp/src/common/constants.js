export const MIN_PROJECT_EVENTS_COUNT = 5;
export const MAX_PROJECT_EVENTS_COUNT = 20;

export const PERIOD_WEEKLY = "weekly";
export const PERIOD_DAILY = "daily";
export const TYPE_MEMBER = "member";
export const TYPE_PROVIDER = "provider";
export const NAMES_MAP = {
    [PERIOD_WEEKLY]: "неделям",
    [PERIOD_DAILY]: "дням",
    [TYPE_MEMBER]: "команде",
    [TYPE_PROVIDER]: "сервису",
};

export const COLORS = [
    "#2f7ed8", "#0d233a", "#8bbc21", "#910000", "#1aadce", "#492970", "#f28f43", "#77a1e5", "#c42525", "#a6c96a",
];
