export default {
    data() {
        return {
            isShow: false,
        };
    },
    methods: {
        close() {
            this.isShow = false;
            this.$bus.emit("close");
        },
    },
    created() {
        this.$bus.on("open", () => {
            this.isShow = true;
        });
    },
}
