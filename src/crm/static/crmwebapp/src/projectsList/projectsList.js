import App from "./App.vue";
import createCrmApp from "../common/appFactory";

const app = createCrmApp(App);

app.mount("#app");
