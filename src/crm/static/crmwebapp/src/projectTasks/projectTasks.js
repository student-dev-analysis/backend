import App from "./App.vue";
import createCrmApp from "../common/appFactory";

if (document.querySelector("#analyticsApp")) {
    const app = createCrmApp(App);
    app.mount("#analyticsApp");
}
