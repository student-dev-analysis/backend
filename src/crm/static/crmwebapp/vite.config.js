import { fileURLToPath, URL } from "url";
import { join, resolve } from "path";
import { defineConfig } from "vite";
import livereload from "livereload";
import vue from "@vitejs/plugin-vue";

function livereloadPlugin() {
    const isProd = process.env.NODE_ENV === "production";
    let server;
    if (!isProd) {
        server = livereload.createServer();
        console.log("start livereload server");
    }
    return {
        name: "livereloadPlugin",
        closeBundle() {
            if (!isProd) {
                server.refresh("/");
            }
        },
    };
}

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue(), livereloadPlugin()],
    resolve: {
        alias: {
            "@": fileURLToPath(new URL("./src", import.meta.url)),
        },
    },
    build: {
        sourcemap: true,
        rollupOptions: {
            input: {
                projectsList: resolve(__dirname, join("src", "projectsList", "index.html")),
                projectTasks: resolve(__dirname, join("src", "projectTasks", "index.html")),
                analysis: resolve(__dirname, join("src", "analysis", "index.html")),
            },
            output: {
                entryFileNames: "[name].js",
                chunkFileNames: "[name].js",
                assetFileNames: "assets/[name].[ext]",
            },
        },
    },
});
