from django import template
from django.contrib.admin.templatetags.admin_list import result_list as admin_list_result_list
from django.contrib.admin.views.main import ChangeList
from django.utils.safestring import mark_safe

from crm.models import CrmProject
from crm.utils import (
    html_links as html_links_util,
    get_project_name_cell,
    get_project_stakeholders_cell,
    get_project_members_cell,
    get_project_services_links,
)

register = template.Library()


@register.simple_tag
def update_variable(value):
    return value


@register.inclusion_tag("crm_admin/projects_change_list_results.html")
def projects_result_list(cl: ChangeList):
    context = admin_list_result_list(cl)
    return context


@register.inclusion_tag("crm_admin/project_tasks_change_list_results.html")
def tasks_result_list(cl: ChangeList):
    context = admin_list_result_list(cl)
    project_id = cl.params.get("project__id__exact")
    project = CrmProject.objects.get(id=project_id) if project_id else None
    context.update(
        {
            "queryset": cl.queryset,
            "project": project,
            "project_name": get_project_name_cell(project) if project else None,
            "project_stakeholders": get_project_stakeholders_cell(project) if project else None,
            "project_members": get_project_members_cell(project) if project else None,
            "project_services": get_project_services_links(project) if project else None,
        }
    )
    return context


@register.filter
def html_links(value):
    if value is None:
        return None
    return mark_safe(html_links_util(value.replace("\n", "<br>")))


@register.filter
def dict_get(h, key):
    return h.get(key, "")
