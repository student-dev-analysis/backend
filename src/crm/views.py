from datetime import datetime

from django.contrib.admin.views.decorators import staff_member_required
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from rest_framework.decorators import api_view
from rest_framework.response import Response

from crm.models import CrmProject
from crm.services.analysis.code import start_code_analysis_for_all_repos
from crm.services.analysis.project_summary import ProjectSummaryService
from crm.services.analysis.projects_last_update_summary import ProjectsLastUpdateSummaryService
from crm.services.analysis.projects_weekly_summary import ProjectsWeeklySummaryService
from crm.services.project_tasks_tabel import get_info
from crm.services.sync_tasks_to_trello import sync_project_tasks
from crm.utils import get_project_name_cell


def start_end_dates_params(func):
    def wrap(request, *args, **kwargs):
        params = request.GET
        start_date = datetime.strptime(params.get("start", None), "%Y-%m-%d") if "start" in params else None
        end_date = datetime.strptime(params.get("end", None), "%Y-%m-%d") if "end" in params else None

        return func(request, *args, start_date=start_date, end_date=end_date, **kwargs)

    return wrap


@staff_member_required
@api_view(["GET"])
@start_end_dates_params
def projects_weekly_summary_api(request, **kwargs):
    project_id = request.GET.get("project_id", None)
    return Response(ProjectsWeeklySummaryService(project_id=project_id, **kwargs).run())


@staff_member_required
@api_view(["GET"])
@start_end_dates_params
def project_weekly_summary_api(request, project_id, **kwargs):
    user_id = request.GET.get("user_id", None)
    return Response(ProjectSummaryService(project_id, user_id=user_id, **kwargs).run())


@staff_member_required
@api_view(["GET"])
def projects_last_update_api(request):
    return Response(ProjectsLastUpdateSummaryService().run())


@staff_member_required
def project_info(request, project_id):
    project = get_object_or_404(CrmProject, id=project_id)
    return HttpResponse(get_project_name_cell(project))


@staff_member_required
def analysis_view(request):
    return render(request, "crm_admin/analysis.html")


@staff_member_required
def project_tasks_tabel_view(request):
    return render(request, "crm_admin/project_tasks_tabel.html", get_info())


@staff_member_required
def project_sync_tasks(request, project_id):
    project = get_object_or_404(CrmProject, id=project_id)
    sync_project_tasks(project.id)
    return redirect(request.META["HTTP_REFERER"])


@staff_member_required
def start_code_analysis(request):
    start_code_analysis_for_all_repos()
    return redirect(request.META["HTTP_REFERER"])
