from django.contrib import admin
from django.db.models import Q
from django.utils.safestring import mark_safe
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from import_export.instance_loaders import ModelInstanceLoader

from core.admin.member import merge_members_action
from core.models import Member
from crm.models import ProjectMember, ProjectStakeholder


class MemberInstanceLoader(ModelInstanceLoader):
    def get_instance(self, row):
        field = self.resource.fields["full_name"]
        # фамилия и имя
        name_lst = field.clean(row).strip().split(" ")[0:2]
        name = " ".join(name_lst).strip()
        name_reversed = " ".join(reversed(name_lst)).strip()
        clause = Q(full_name__icontains=name) | Q(full_name__icontains=name_reversed)

        try:
            field = self.resource.fields.get("gmail")
            gmail = field.clean(row)
            if gmail:
                clause |= Q(gmail__icontains=gmail)
        except KeyError:
            pass

        return self.get_queryset().filter(clause).first()


class MemberResource(resources.ModelResource):
    class Meta:
        model = Member
        # список не участвует в условиях и нужен, чтобы просто запустить поиск дубликатов
        import_id_fields = ("full_name",)
        instance_loader_class = MemberInstanceLoader
        exclude = ("created_at", "updated_at")


class ProjectMemberInline(admin.TabularInline):
    model = ProjectMember
    verbose_name = "Участник команд"


class ProjectStakeholderInline(admin.TabularInline):
    model = ProjectStakeholder
    verbose_name = "Заинтересованное лицо в проектах"


class MemberAdmin(ImportExportModelAdmin):
    resource_class = MemberResource
    list_display = (
        "id",
        "full_name",
        "get_group_or_subdivision",
        "email",
        "gmail",
        "get_gitlab",
        "get_trello",
        "get_telegram",
        "created_at",
    )
    actions = [merge_members_action]
    inlines = [ProjectMemberInline, ProjectStakeholderInline]
    search_fields = ("full_name", "email", "gmail")
    list_filter = ("group", "subdivision")
    readonly_fields = ("get_gitlab", "get_trello", "get_telegram")
    exclude = ("project_repository_user", "project_board_user", "project_chat_user")
    ordering = ("-updated_at",)
    list_editable = ("full_name",)

    @admin.display(description="Группа или подразделение")
    def get_group_or_subdivision(self, instance):
        return instance.subdivision or instance.group

    @admin.display(description="Gitlab")
    def get_gitlab(self, instance):
        if instance.project_repository_user is not None:
            l = instance.project_repository_user.login
            return mark_safe(f'<a href="https://gitlab.com/{l}" target="_blank">{l}</a>')
        return "-"

    @admin.display(description="Trello")
    def get_trello(self, instance):
        if instance.project_board_user is not None:
            l = instance.project_board_user.login
            return mark_safe(f'<a href="https://trello.com/{l}" target="_blank">{l}</a>')
        return "-"

    @admin.display(description="Telegram")
    def get_telegram(self, instance):
        if instance.project_chat_user is not None:
            l = instance.project_chat_user.login
            return mark_safe(f'<a href="https://t.me/{l}" target="_blank">{l}</a>')
        return "-"

    def get_queryset(self, request):
        return (
            super(MemberAdmin, self)
            .get_queryset(request)
            .select_related("project_repository_user", "project_board_user", "project_chat_user")
        )
