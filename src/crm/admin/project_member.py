from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.urls import reverse
from django.utils.safestring import mark_safe


class ProjectMemberAdmin(ModelAdmin):
    list_display_links = ("comment", "created_at")
    search_fields = ("project__name", "member__full_name")
    list_filter = ("project", "created_at")
    list_display = ("get_project", "get_member", "comment", "created_at")
    ordering = ("project", "created_at")

    @admin.display(description="Проект")
    def get_project(self, instance):
        url = reverse("admin:crm_crmproject_change", args=(instance.project_id,))
        return mark_safe(f'<a href="{url}">{instance.project}</a>')

    @admin.display(description="Контакт")
    def get_member(self, instance):
        url = reverse("admin:crm_crmmember_change", args=(instance.member_id,))
        return mark_safe(f'<a href="{url}">{instance.member}</a>')

    def get_queryset(self, request):
        return super(ProjectMemberAdmin, self).get_queryset(request).select_related("project", "member")
