from core.admin import admin_site
from crm.admin.member import MemberAdmin
from crm.admin.project import ProjectAdmin
from crm.admin.project_member import ProjectMemberAdmin
from crm.admin.project_task import ProjectTaskAdmin
from crm.models import CrmProject, CrmMember, ProjectMember, CrmProjectTechnology, ProjectTask

admin_site.register(CrmProject, ProjectAdmin)
admin_site.register(CrmMember, MemberAdmin)
admin_site.register(ProjectMember, ProjectMemberAdmin)
admin_site.register(CrmProjectTechnology)
admin_site.register(ProjectTask, ProjectTaskAdmin)
