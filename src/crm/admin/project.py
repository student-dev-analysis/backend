from django import forms
from django.contrib import admin
from django.db.models import Prefetch
from django.urls import reverse
from django.utils.safestring import mark_safe
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from tablib import Dataset

from core.enums import ProjectStatus, ProjectImportance
from crm.constants import PROJECT_ORDERING
from crm.utils import (
    html_links,
    get_project_name_cell,
    get_project_members_cell,
    get_project_stakeholders_cell,
    get_project_services_links,
)
from crm.models import (
    ProjectMember,
    ProjectComment,
    ProjectStakeholder,
    CrmMember,
    CrmProject,
    CrmProjectTechnology,
    ProjectTask,
)


class ProjectTaskInline(admin.TabularInline):
    model = ProjectTask


class ProjectMemberForm(forms.ModelForm):
    member = forms.ModelChoiceField(CrmMember.objects.filter(is_student=True))

    class Meta:
        model = ProjectMember
        fields = ("member", "comment", "is_old")


class ProjectMemberInline(admin.TabularInline):
    model = ProjectMember
    form = ProjectMemberForm
    verbose_name = "Участник"
    verbose_name_plural = "Команда"


class ProjectStakeholderForm(forms.ModelForm):
    member = forms.ModelChoiceField(CrmMember.objects.filter(is_student=False))

    class Meta:
        model = ProjectStakeholder
        fields = ("member", "comment")


class ProjectStakeholderInline(admin.TabularInline):
    model = ProjectStakeholder
    form = ProjectStakeholderForm


class ProjectCommentInline(admin.TabularInline):
    model = ProjectComment
    readonly_fields = ("author",)

    def has_change_permission(self, request, obj=None):
        return False


class ProjectResource(resources.ModelResource):
    def before_import_row(self, row: dict, row_number=None, **kwargs):
        techs = row.get("technologies", None)
        if techs:
            techs = techs.split(",")
            new_techs = []
            tech_ids = []
            for tech in techs:
                if tech.lower() in self._technologies_map:
                    tech_ids.append(self._technologies_map[tech.lower()])
                else:
                    new_techs.append(CrmProjectTechnology(name=tech))

            for new_model in CrmProjectTechnology.objects.bulk_create(new_techs):
                tech_ids.append(new_model.id)
                self._technologies_map[new_model.name.lower()] = new_model.id

            row["technologies"] = ",".join(map(str, tech_ids))

        status = row.get("status", None)
        if status:
            row["status"] = self._status_title_slug_map.get(status.strip().lower(), row["status"])

        status = row.get("importance", None)
        if status:
            row["importance"] = self._importance_title_slug_map.get(status.strip().lower(), row["importance"])

    def before_import(self, dataset: Dataset, using_transactions, dry_run, **kwargs):
        self._technologies_map = {o.name.lower(): o.id for o in CrmProjectTechnology.objects.all()}
        self._status_title_slug_map = {title.lower(): slug for slug, title in ProjectStatus.choices}
        self._importance_title_slug_map = {title.lower(): slug for slug, title in ProjectImportance.choices}

    class Meta:
        model = CrmProject
        # список не участвует в условиях и нужен, чтобы просто запустить поиск дубликатов
        import_id_fields = ("name",)
        exclude = ("created_at", "updated_at")


class ProjectAdmin(ImportExportModelAdmin):
    resource_class = ProjectResource
    list_display = ("get_name", "get_links", "get_stakeholders", "get_members", "get_tasks")
    search_fields = ("name", "description")
    inlines = [ProjectCommentInline, ProjectTaskInline, ProjectMemberInline, ProjectStakeholderInline]
    list_filter = (
        "employee_curator",
        "status",
        "importance",
        "course",
    )
    ordering = PROJECT_ORDERING
    change_list_template = "crm_admin/projects_change_list.html"
    change_form_template = "crm_admin/projects_change_form.html"
    readonly_fields = ("get_services_links",)

    def render_change_form(self, request, context, add=False, change=False, form_url="", obj=None):
        context.update(
            {
                "title": getattr(context["original"], "name", None),
                "projects": CrmProject.objects.all().order_by(*self.ordering),
            }
        )
        return super(ProjectAdmin, self).render_change_form(request, context, add, change, form_url, obj)

    @admin.display(description="Название", ordering="name")
    def get_name(self, instance: CrmProject):
        return get_project_name_cell(instance, with_tasks_link=True)

    @admin.display(description="Ссылки")
    def get_links(self, instance):
        if instance.links is None:
            return None
        text = html_links(instance.links).replace("\n", "<br>")
        return get_project_services_links(instance) + mark_safe("<br>" + text)

    @admin.display(description="Заинтересованные лица")
    def get_stakeholders(self, instance: CrmProject):
        return get_project_stakeholders_cell(instance)

    @admin.display(description="Команда")
    def get_members(self, instance: CrmProject):
        return get_project_members_cell(instance)

    @admin.display(description="Задачи")
    def get_tasks(self, instance: CrmProject):
        html = ""
        for task in instance.tasks.all():
            html += f'<div class="{"task-completed" if task.is_completed else ""}">- {html_links(str(task))}</div>'
        return mark_safe(html)

    @admin.display(description="Ссылки на сервисы")
    def get_services_links(self, instance: CrmProject):
        core_project_link = reverse("admin:core_project_change", args=(instance.id,))
        core_project_link_html = mark_safe(f'<small><a href="{core_project_link}">Изменить</a></small>')
        return get_project_services_links(instance) + core_project_link_html

    def get_queryset(self, request):
        return (
            super(ProjectAdmin, self)
            .get_queryset(request)
            .select_related("employee_curator")
            .prefetch_related(
                "projectstakeholder_set",
                "projectstakeholder_set__member",
                Prefetch("projectmember_set", ProjectMember.objects.filter(is_old=False).select_related("member")),
                Prefetch("tasks", ProjectTask.objects.all().filter(is_completed=False)),
                "boards",
                "repositories",
            )
        )

    def save_formset(self, request, form, formset, change):
        instances = formset.save()
        for instance in instances:
            if isinstance(instance, ProjectComment):  # Check if it is the correct type of inline
                instance.author_id = request.user.id
                instance.save()
