from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.db.models import Count, Q, F
from django.shortcuts import redirect
from django.urls import reverse
from django.utils.safestring import mark_safe

from core.enums import ProjectStatus
from crm.constants import PROJECT_ORDERING
from crm.models import CrmProject
from crm.utils import html_links


class ProjectTaskAdmin(ModelAdmin):
    search_fields = ("project__name", "title")
    list_filter = (
        "project__employee_curator",
        "created_at",
    )
    list_display = ("id", "get_project", "is_completed", "get_title", "date", "mark")
    ordering = ("project__importance", "project", "is_completed", F("date").asc(nulls_last=True))
    change_list_template = "crm_admin/project_tasks_change_list.html"

    def changelist_view(self, request, extra_context=None):
        extra_context = extra_context or {}
        extra_context.update(
            {
                "projects": CrmProject.objects.all()
                .annotate(
                    completed_tasks_count=Count("tasks", filter=Q(tasks__is_completed=True)),
                    tasks_count=Count("tasks"),
                )
                .order_by(*PROJECT_ORDERING)
            }
        )
        return super(ProjectTaskAdmin, self).changelist_view(request, extra_context)

    @admin.display(description="Проект")
    def get_project(self, instance):
        url = reverse("admin:crm_crmproject_change", args=(instance.project_id,))
        return mark_safe(f'<a href="{url}">{instance.project}</a>')

    @admin.display(description="Задача")
    def get_title(self, instance):
        return mark_safe(html_links(instance.title))

    def get_queryset(self, request):
        qs = super(ProjectTaskAdmin, self).get_queryset(request).select_related("project")
        if "project__id__exact" not in request.GET:
            qs = qs.filter(is_completed=False).exclude(project__status=ProjectStatus.done)
        return qs

    def response_add(self, request, obj, post_url_continue=None):
        return redirect(reverse("admin:crm_projecttask_changelist") + f"?project__id__exact={obj.project_id}")
