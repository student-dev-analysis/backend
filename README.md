# core

- `docker-compose up -d` - запуск контейнеров для разработки проекта
- `poetry install` - установка зависимостей
- `poetry shell` - вход в virtualenv
- `python src/manage.py migrate` - запуск миграций
- `python src/manage.py runserver` - запуск сервера для разработки
- `python src/manage.py createsuperuser` - создание суперпользователя
- `pip3 install pre-commit && pre-commit install` - Установка pre-commit hook для автоматического запуска линтера
- `black src` - Ручной запуск линтера по всему проекту
- `cd src/crm/static/crmwebapp` - переход в папку с фронтендом
- `npm run watch` - сборка фронтенда для разработки

Grafana: http://localhost:3000, логин и пароль - `admin`

## OAuth credentials

### Trello

- Зайти на https://trello.com/app-key
- Узнать KEY и SECRET и записать в переменные `SOCIAL_AUTH_TRELLO_KEY`, `SOCIAL_AUTH_TRELLO_SECRET`
- Установить домен приложения как разрешенный
