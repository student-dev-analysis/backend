#!/bin/bash

# скрипт для копирования данных таблиц clickhouse с сервера на локальную машину.
# использование: ./deploy/download_clickhouse_table_to_local.sh TABLENAME

LIMIT=100000
DUMP_PATH=/srv/clickhouse_downloading_to_local_dump.csv
DUMP_LOCAL_PATH=/tmp/clickhouse_downloading_to_local_dump.csv
SERVER_NAME=sda.atnartur.ru
DB_NAME=studentdevanalysis

echo "table name is '$1'"
TABLE_NAME=$1

echo creating dump

SQL="SELECT * FROM $TABLE_NAME LIMIT $LIMIT FORMAT CSV"
ssh root@$SERVER_NAME "clickhouse-client -d $DB_NAME --query='$SQL' > $DUMP_PATH"

echo downloading dump
scp root@$SERVER_NAME:$DUMP_PATH $DUMP_LOCAL_PATH

CLICKHOUSE_CONTAINER_ID=$(docker ps | grep clickhouse | awk '{print $1}')

echo cleaning up local table
docker exec -i $CLICKHOUSE_CONTAINER_ID clickhouse-client -d $DB_NAME --query="ALTER TABLE $TABLE_NAME DELETE WHERE 1=1"

echo uploading dump
cat $DUMP_LOCAL_PATH | docker exec -i $CLICKHOUSE_CONTAINER_ID clickhouse-client -d $DB_NAME --query="INSERT INTO $TABLE_NAME FORMAT CSV"

COUNT=$(docker exec -i $CLICKHOUSE_CONTAINER_ID clickhouse-client -d $DB_NAME --query="SELECT count() FROM $TABLE_NAME")

echo table now contains $COUNT rows

echo removing files
ssh root@$SERVER_NAME rm $DUMP_PATH
rm $DUMP_LOCAL_PATH
