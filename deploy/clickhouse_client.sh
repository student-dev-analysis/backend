#!/bin/bash
CONTAINER_ID=$(docker ps | grep clickhouse | awk '{print $1}')
docker exec -ti $CONTAINER_ID clickhouse-client -d studentdevanalysis
