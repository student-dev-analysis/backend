#!/bin/bash
cd src
until celery -A analysis inspect ping; do
    >&2 echo "Celery workers not available"
    sleep 10
done

echo 'Starting flower'
celery -A analysis flower --address=localhost --url_prefix=flower
